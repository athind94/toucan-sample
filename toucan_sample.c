/*
===============================================================================
 Name        : toucan_full_test.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/


#include <cr_section_macros.h>
#include <math.h>

#include <toucan.h>
#include <can.h>
#include <error.h>
#include <gpio.h>
#include <timer.h>
#include <uart.h>

#define PI 3.14159265

#define LED_PORT_1	0
#define LED_PIN_1	28

#define LED_PORT_2	0
#define LED_PIN_2	3

#define CHANNEL_1	1
#define CHANNEL_2	2

int test_handler_calls;
int num_interrupts;

void test_handler(_S32 data, tc_time_t timestamp)
{
	test_handler_calls++;
	tc_gpio_togglePinValue(LED_PORT_2, LED_PIN_2);
}

void test_handler2(void)
{
	num_interrupts++;
	tc_gpio_togglePinValue(LED_PORT_1, LED_PIN_1);
}

int main(void) {

    tc_init(97);


    tc_gpio_setPinDir(LED_PORT_1, LED_PIN_1, TC_GPIO_DIR_OUTPUT);
    tc_gpio_setPinDir(LED_PORT_2, LED_PIN_2, TC_GPIO_DIR_OUTPUT);

    _S32 angle = 0;

    tc_assign_inbox_handler(MSG_BROADCAST, 97, CHANNEL_1, test_handler);
    tc_assign_inbox_handler(MSG_BROADCAST, 97, CHANNEL_2, test_handler);

    while(1) {
    	tc_handle();

    	tc_timer_delay_ms(200);
    	tc_gpio_togglePinValue(LED_PORT_1, LED_PIN_1);

    	tc_send_broadcast(P_TELEMETRY, CHANNEL_1, (_S32)(1000 + 1000*sin(angle*(PI/180))));
    	tc_send_broadcast(P_TELEMETRY, CHANNEL_2, (_S32)(1000 + 1000*sin((2*angle)*(PI/180))));

    	angle++;
    	angle = angle % 360;
    }

    return 0 ;
}
